# -*- coding: utf-8 -*-
"""
Created on Sat Mar 10 19:11:19 2018

@author: Lyndon Teng
"""
import json
from sorter import product
from file_input import new_item

working_directory="D:\OneDrive - University Of Cambridge\Attachments\Test Set"
file = working_directory+"\img.jpg"
year=2016 #Test year

with open('cache.json', 'r') as infile:  
    laptops=json.load(infile)

print(laptops)

products=[]

for i in laptops:
    product.type_of_product=i[0]
    product.brand=i[1]
    product.date_of_manufacture=i[2]
    products.append(product)

new_laptops=products.append(new_item(file, year))

with open('cache.json', 'w') as outfile:  
    json.dump(new_laptops, outfile)