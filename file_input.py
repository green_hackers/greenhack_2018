# -*- coding: utf-8 -*-
"""
Created on Sat Mar 10 16:39:38 2018

@author: Lyndon Teng
"""

from sorter import product
from Image_classifier import image_categoriser
from Custom_vision import brand_sorter

#working_directory="D:\OneDrive - University Of Cambridge\Attachments\Test Set"

#output=[]

#file = working_directory+"\img"
#year=2002 #Test year

def new_item(file, year):
    item=product(image_categoriser(file), brand_sorter(file), year)
    return item