# -*- coding: utf-8 -*-
"""
Created on Sat Mar 10 14:23:56 2018

@author: Lyndon Teng
"""
import requests
import json

#test_file="D:\OneDrive - University Of Cambridge\Attachments\Test Set\huawei.jpeg"

def brand_sorter(test_file):
    #Connect to Azure
    url="https://southcentralus.api.cognitive.microsoft.com/customvision/v1.1/Prediction/add39524-8755-4c9e-8943-f00b201f33f4/image?iterationId=c9f6199c-457b-422e-a783-6c8fc30e355c"
    headers={'content-type':'application/octet-stream','Prediction-Key':'c799e22cdac84ef3a5a122b953149ffc'}
    
    #Upload Test Image
    r =requests.post(url,data=open(test_file,"rb"),headers=headers)
    
    #Decode Data from binary
    data = r.content.decode()
    dictionary=json.loads(data)
    
    #Slice tag with highest probability
    return dictionary["Predictions"][0]["Tag"]