from sorter import product

laptops = [
        product("laptop", "dell", 2012),
        product("laptop", "dell", 2013),
        product("laptop", "dell", 2012),
        product("laptop", "dell", 2013),
        product("laptop", "Apple", 2014),
        product("laptop", "Apple", 2012),
        product("laptop", "Apple", 1992),
        product("laptop", "Apple", 1993),
        product("laptop", "dell", 1999),
        product("laptop", "dell", 1999),
        ]
