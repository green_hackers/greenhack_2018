#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 10 13:38:44 2018

@author: Sehaj
"""
import datetime
now = datetime.datetime.now()

class product:
    
    def __init__(self, type_of_product , brand, date_of_manufacture):
        self.type_of_product = type_of_product
        self.brand = brand
        self.date_of_manufacture = date_of_manufacture
        self.date_of_entry = now.year
        self.life_length = None
        
    def calculate_life_length(self):
        length_in_years = 0
        length_in_years = self.date_of_entry-self.date_of_manufacture
        self.life_length = length_in_years
        
def sort_by_type(product):
    laptops = []
    speakers = []
    phones = []
    if product.type_of_product == "laptop":
        laptops.append(product)
    if product.type_of_product == "phone":
        phones.append(product)
    if product.type_of_product == "speakers":
        speakers.append(product)


def sort_by_brand_for_laptops(laptops):
    brands = []
    numbers = []
    life_length = []
    for j in laptops:
        if j.brand in brands:
            for i in range(0, len(brands)):
                if j.brand == brands[i]:
                    numbers[i] = numbers[i] + 1
                    life_length[i] = (life_length[i]+ j.life_length)/2
        else:
            brands.append(j.brand)
            numbers.append(1)
            life_length.append(j.life_length)
    return (brands, life_length)

def sort_by_brand_for_phones(phones):
    brands = []
    numbers = []
    life_length = []
    for j in phones:
        if j.brand in brands:
            for i in range(0, len(brands)):
                if j.brand == brands[i]:
                    numbers[i] = numbers[i] + 1
                    life_length[i] = (life_length[i]+ j.life_length)/2
        else:
            brands.append(j.brand)
            numbers.append(1)
            life_length.append(j.life_length)
    return brands, numbers, life_length
            
            
def sort_by_brand_for_speakers(speakers):
    brands = []
    numbers = []
    life_length = []
    for j in speakers:
        if j.brand in brands:
            for i in range(0, len(brands)):
                if j.brand == brands[i]:
                    numbers[i] = numbers[i] + 1
                    life_length[i] = (life_length[i]+ j.life_length)/2
        else:
            brands.append(j.brand)
            numbers.append(1)
            life_length.append(j.life_length)   
    return brands, numbers, life_length



        
