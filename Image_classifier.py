# -*- coding: utf-8 -*-
"""
Created on Sat Mar 10 12:03:59 2018

@author: Lyndon Teng
"""
#initialise libraries
import requests
import json

#Setup Connection to Azure
subscription_key = "f6f25296af5247faacb78db262465bdc"
assert subscription_key

vision_base_url = "https://westcentralus.api.cognitive.microsoft.com/vision/v1.0/"
vision_analyze_url = vision_base_url + "analyze"

#Image to be analysed
#image_url = "https://www.webantics.com/content/images/thumbs/004/0046321_huawei-hg659-dual-band-ac1300-adsl2vdsl2fibre-wireless-ac-router-fibre-ready-with-4g-capability_600.jpeg"

test_image="D:\OneDrive - University Of Cambridge\Attachments\Test Set\download (3).jpg"

def image_categoriser(test_image):
    type_of_appliance=None
    #Object Categoriser
    headers  = {'Ocp-Apim-Subscription-Key': subscription_key, 'Content-Type': 'application/octet-stream' }
    params   = {'visualFeatures': 'Categories,Description,Color'}
    data = open(test_image, 'rb')
    response = requests.post(vision_analyze_url, headers=headers, params=params, data=data)
    content = response.content.decode()
    analysis =json.loads(content)
    
    #Retrieve image caption"
    image_caption = analysis["description"]["tags"]
    
    if image_caption==None:
        raise RuntimeError("Are you sure this is an appliance?")
    
    else: 
        for tag in image_caption:
            if tag in ["laptop", "computer"]:
                type_of_appliance="laptop"
            elif tag in ["phone", "cellphone", "smartphone", "handphone"]:
                if type_of_appliance=="laptop":
                    raise Exception("It can't be two things at once! try taking another photo!")
                else:
                    type_of_appliance="phone"
            if tag in ["speaker", "loudspeaker"]:
                if type_of_appliance=="phone":
                    raise Exception("It can't be two things at once! try taking another photo!")
                else:
                    type_of_appliance="speaker"
                
    return type_of_appliance
