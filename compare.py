from sorter import product
from sorter import sort_by_type
from sorter import sort_by_brand_for_laptops
from sorter import sort_by_brand_for_phones
from sorter import sort_by_brand_for_speakers
from file_input import new_item

working_directory="D:\OneDrive - University Of Cambridge\Attachments\Test Set"

laptops = [
        product("laptop", "dell", 2012),
        product("laptop", "dell", 2013),
        product("laptop", "dell", 2012),
        product("laptop", "dell", 2013),
        product("laptop", "Apple", 2014),
        product("laptop", "Apple", 2012),
        product("laptop", "Apple", 1992),
        product("laptop", "Apple", 1993),
        product("laptop", "dell", 1999),
        product("laptop", "dell", 1999),
        ]


file = working_directory+"\img.jpg"
year=2003 #Test year

laptops.append(new_item(file, year))

def sorted_by_key(x, i, reverse=False):
    """For a list of lists/tuples, return list sorted by the ith
    component of the list/tuple, E.g.

    Sort on first entry of tuple:

      > sorted_by_key([(1, 2), (5, 1]), 0)
      >>> [(1, 2), (5, 1)]

    Sort on second entry of tuple:

      > sorted_by_key([(1, 2), (5, 1]), 1)
      >>> [(5, 1), (1, 2)]

    """

    # Sort by distance
    def key(element):
        return element[i]

    return sorted(x, key=key, reverse=reverse)


def compile_list(brands, life_length):
    output_list = []
    for i in range(0, len(brands)):
        output_list.append((brands[i],life_length[i]))
    return output_list

def sorted_list(brands, life_length):
    lister = compile_list(brands, life_length)
    lister = sorted_by_key(lister, 1, True)
    print("The brands and their respective acerage life cycle lengths are as follows (in descending order): " + str(lister))

for i in range(0, len(laptops)):
    product.calculate_life_length(laptops[i])

brands1, life_len1 = sort_by_brand_for_laptops(laptops)
print(sorted_list(brands1, life_len1))